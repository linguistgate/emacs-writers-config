;;; -*- no-byte-compile: t -*-
(define-package "easy-hugo" "20180801.1522" "Write blogs made with hugo by markdown or org-mode" '((emacs "24.4") (popup "0.5.3")) :commit "bcebd371b70a592d1a74169de91b6102e06265a3" :authors '(("Masashı Mıyaura")) :maintainer '("Masashı Mıyaura") :url "https://github.com/masasam/emacs-easy-hugo")
