(define-package "ebib" "20180730.2103" "a BibTeX database manager"
  '((dash "2.5.0")
    (seq "2.15")
    (parsebib "2.3")
    (emacs "24.4"))
  :keywords
  '("text" "bibtex")
  :authors
  '(("Joost Kremers" . "joostkremers@fastmail.fm"))
  :maintainer
  '("Joost Kremers" . "joostkremers@fastmail.fm"))
;; Local Variables:
;; no-byte-compile: t
;; End:
