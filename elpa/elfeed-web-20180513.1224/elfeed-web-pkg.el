(define-package "elfeed-web" "20180513.1224" "web interface to Elfeed"
  '((simple-httpd "1.4.3")
    (elfeed "1.4.0")
    (emacs "24.1")))
;; Local Variables:
;; no-byte-compile: t
;; End:
